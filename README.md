# What is DotSpatial?

DotSpatial is a geographic information system library written for .NET 4. It allows developers to incorporate spatial data, analysis and mapping functionality into their applications or to contribute GIS extensions to the community. DotSpatial provides a map control for .NET and several GIS capabilities including: 

* Display a map in a .NET Windows Forms or Web application.
* Open shapefiles, grids, rasters and images.
* Render symbology and labels
* Reproject on the fly
* Manipulate and display attribute data
* Scientific analysis
* Read GPS data

Who uses DotSpatial?

* MapWindow 6
* HydroDesktop
* MAD
* LineSiter
* Marine Life
* PVMapper Site Designer

**This is a fork of dotspatial.  Please reference the official site on CodePlex: http://dotspatial.codeplex.com/**

**dotspatial is also available as a nuget package.**